### Installation ### 

Add this repository to composer.json:
```
#!json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/sc0nf1/casinoapiclient.git"
        }
    ]
}
```
Run installation:
```
#!console
composer require sc0nf1/casino-api-client @dev
```
Read more about [https://getcomposer.org/doc/00-intro.md](Composer)

### Usage ###
```
#!php

<?php

use newgen\casino\api\client\Client;

$client = new Client(array('sslKeyPath' => '/path/to/apikey.pem', 'url' => 'https://apigame.game-host.org/'));

// check API version
print_r($client->apiVersion());

// register player
print_r($client->setPlayerInfo('user234'));
// or print_r($client->setPlayerInfo(array('player_id' => 'user234', 'nick' => 'user234')));

// check balance
print_r($client->getBalance('user234'));

// deposit balance
print_r($client->changeBalance('user234', 15));

// list games
$games = $client->listGames();
print_r($games);

// run the game
print_r($client->runGame($games[0]['id'], 'user234'));
```